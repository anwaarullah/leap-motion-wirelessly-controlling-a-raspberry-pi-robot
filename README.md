This repo contains the Source Codes (HTML and Python) running off the WebIOPi IoT framework and Wireless Leap Motion integration code embedded within the HTML folder.

The entire project writeup and build instructions are available here: [https://www.hackster.io/Anwaarullah/leap-motion-controlled-remote-search-and-disposal-robot-4cb662](Link URL)

This project is submitted as an entry in the Leap Motion #3DJam 2015