import webiopi
import datetime
import subprocess

GPIO = webiopi.GPIO

L1 = 9 # GPIO pin using BCM numbering
L2 = 11
L3= 23
L4= 24
# setup function is automatically called at WebIOPi startup
def setup():
    # set the GPIO used by the light to output
	GPIO.setFunction(L1, GPIO.OUT)
	GPIO.setFunction(L2, GPIO.OUT)
	GPIO.setFunction(L3, GPIO.OUT)
	GPIO.setFunction(L4, GPIO.OUT)
# loop function is repeatedly called by WebIOPi 
def loop():
    # retrieve current datetime
	webiopi.sleep(1)

# destroy function is called at WebIOPi shutdown
def destroy():
	GPIO.digitalWrite(L1, GPIO.LOW)
	GPIO.digitalWrite(L2, GPIO.LOW)
	GPIO.digitalWrite(L3, GPIO.LOW)
	GPIO.digitalWrite(L4, GPIO.LOW)

	
	
@webiopi.macro
def shutDown():
	subprocess.call(["sudo","shutdown","-h","now"])
	
	
@webiopi.macro
def go_forward():
	GPIO.digitalWrite(L1, GPIO.HIGH)
	GPIO.digitalWrite(L3, GPIO.HIGH)
	GPIO.digitalWrite(L2, GPIO.LOW)
	GPIO.digitalWrite(L4, GPIO.LOW)

@webiopi.macro
def go_back():
	GPIO.digitalWrite(L2, GPIO.HIGH)
	GPIO.digitalWrite(L4, GPIO.HIGH)
	GPIO.digitalWrite(L1, GPIO.LOW)
	GPIO.digitalWrite(L3, GPIO.LOW)
	
@webiopi.macro
def go_left():
	GPIO.digitalWrite(L1, GPIO.HIGH)
	GPIO.digitalWrite(L4, GPIO.HIGH)
	GPIO.digitalWrite(L2, GPIO.LOW)
	GPIO.digitalWrite(L3, GPIO.LOW)
	
	
@webiopi.macro
def go_right():
	GPIO.digitalWrite(L2, GPIO.HIGH)
	GPIO.digitalWrite(L3, GPIO.HIGH)
	GPIO.digitalWrite(L1, GPIO.LOW)
	GPIO.digitalWrite(L4, GPIO.LOW)
	
@webiopi.macro
def halt():
	GPIO.digitalWrite(L1, GPIO.LOW)	
	GPIO.digitalWrite(L2, GPIO.LOW)
	GPIO.digitalWrite(L3, GPIO.LOW)
	GPIO.digitalWrite(L4, GPIO.LOW)
